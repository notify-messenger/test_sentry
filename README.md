# test_sentry

This project is a minimal reproducible test of our Sentry stacktrace issues.
When the button is pressed and the error is reported to sentry, 
the file and line number it reports is completely wrong.

## Reproduction steps
Follow the below steps to run the project and reproduce the issue.

First edit `src/main.ts` and set the Sentry DSN.

```
# CD into the project and use nvm to select the correct project
nvm use

# Install packages
yarn install

# Run the build script
./scripts/cf_build.sh

# Switch to a newer node version, you may have to install it first.
nvm use lts/hydrogen

# Install http-server
npm install -g http-server

# Run the project
http-server ./dist
```

Once the project is running, click the button and notice that an error is reported to the console. In sentry, 
the reported stacktrace is wrong.
