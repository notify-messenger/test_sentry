import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

import 'hammerjs';
import * as Sentry from '@sentry/angular';

Sentry.init({
	dsn: 'YOUR_DSN_HERE',
});

enableProdMode();
platformBrowserDynamic()
	.bootstrapModule(AppModule)
	.then()
	.catch((err) => {
		console.log('failed to bootstrap: ' + err);
	});
