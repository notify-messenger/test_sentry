import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgmCoreModule } from '@agm/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { APP_BASE_HREF } from '@angular/common';
import { SWIPER_CONFIG, SwiperConfigInterface, SwiperModule } from 'ngx-swiper-wrapper';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { KeyboardShortcutsModule } from 'ng-keyboard-shortcuts';
import * as Sentry from '@sentry/angular';

// uncomment when app uses formatDate and so on
//import {LOCALE_ID} from '@angular/core';
//import {HttpService} from './services/http-service';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
	direction: 'horizontal',
	slidesPerView: 'auto',
};

const appRoutes: Routes = [];

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		ReactiveFormsModule,
		FormsModule,
		HttpClientModule,
		SwiperModule,
		KeyboardShortcutsModule.forRoot(),
		MatDialogModule,
		MatButtonModule,
		RouterModule.forRoot(appRoutes, {
			enableTracing: false,
		}),
		AgmCoreModule.forRoot({
			apiKey: 'AIzaSyB-PvmYU5y4GQXh1aummcUI__LNhCtI68o',
			libraries: ['places'],
		}),
		StoreDevtoolsModule.instrument({}),
		HammerModule,
	],
	providers: [
		{ provide: APP_BASE_HREF, useValue: '/' },
		{ provide: SWIPER_CONFIG, useValue: DEFAULT_SWIPER_CONFIG },
		{
			provide: ErrorHandler,
			useValue: Sentry.createErrorHandler({
				showDialog: true,
			}),
		},
	],
	// Boot strapping happens in main.ts
	bootstrap: [],
})
export class AppModule {
	ngDoBootstrap(app) {
		console.log('normal bootstrap');
		const appRoot = document.createElement('app-root', {});
		appRoot.style.flexGrow = '1';
		appRoot.style.display = 'flex';
		appRoot.style.flexDirection = 'column';
		document.body.appendChild(appRoot);

		app.bootstrap(AppComponent);
	}
}
