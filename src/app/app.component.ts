import { AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, Renderer2, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter as _filter } from 'lodash';
import { BehaviorSubject, combineLatest, fromEvent, merge, Observable, of, ReplaySubject, Subject, throwError, zip } from 'rxjs';

import {
	catchError,
	concatMap,
	distinctUntilChanged,
	filter,
	finalize,
	map,
	mergeMap,
	switchMap,
	take,
	takeUntil,
	tap,
	withLatestFrom,
} from 'rxjs/operators';

import { OverlayContainer } from '@angular/cdk/overlay';
import { DomSanitizer, SafeResourceUrl, Title } from '@angular/platform-browser';
import { ShortcutInput } from 'ng-keyboard-shortcuts';
import _refiner from 'refiner-js';
import { HttpErrorResponse } from '@angular/common/http';

declare const window;
declare let ResizeObserver;

export const INITIAL_LOCATION_PATHNAME = new ReplaySubject<string>(1);

/**
 * @title Autocomplete overview
 */
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	throwAnError() {
		let obj: any;
		console.log(obj.nonExistentProperty); // This will produce an error
	}
}
